
import networkx as nx
from regraph import NXGraph, Rule, plot_rule

 
from enum import Enum

class TARGET_LANG(Enum):

    SPARQL = "SPARQL"
    GRAPH_PATTERN = "GRAPH_PATTERN"
    ENTITY = "ENTITY"

class OPERATOR(Enum):

    EQ = "EQ"
    NE = "NE"
    GT = "GT"
    LT = "LT"
    STARTSWITH = "STARTSWITH"
    ENDSWITH = "ENDSWITH"
    CONTAINS = "CONTAINS"

class QueryBuilder():

  def __init__(self):
    self.elements=dict()
    self.toolBoxes=dict()
    self.defaultToolBox=None
    self.g = nx.Graph() 
    self.op=self.operators()
    self.add_children=True
    self.nid2e=dict()
    self.param_var_masked=dict() # to list all variables
    self.param_var_unmasked=dict()
    self.vmask="__MSK__"

  def addToolBox(self,tb):
    self.toolBoxes[tb.target_language]=tb
    self.defaultToolBox=tb
  
  def operators(self):
     lst=[]
     for k,v in OPERATOR._member_map_.items():
      lst.append(k)
     return lst
  
  def add(self,element):
     nid="e"+str(len( self.elements.keys()))
     if  element.label is None:
        element.label=nid
     element.nid=nid
     self.g.add_node(nid,label=element.label)
     self.elements[element.label]=element
     self.nid2e[element.nid]=element
        
  def selectIndexes(self):
     return self.defaultToolBox.selectpath_indexes()

  def connected_components_count(self):
    
    cct=0
    for c in  nx.connected_components(self.g):
      cct+=1
     
    return cct
  
  def edge_attributes(self,k):
    al=None
    eattrl=list(list( self.g.edges(data=True))[0][-1].keys())
    if k in  eattrl:
       al = nx.get_edge_attributes(self.g, k)  
    return al  

  def edge_id(self,s,t):
    eid="%s_%s" %(s,t)
    return eid 
  def mask(self,v):
     return "%s%s%s" %(self.vmask,v,self.vmask)
  
  def simplenames(self,code):
     umdict={}
     umdict_ct={}
     for k in self.param_var_masked.keys():
        uk=k.replace(self.vmask,"")
        skl=uk.split("__")
        sk=skl[0]
        ct=0
        if sk in umdict_ct.keys():
           ct=umdict_ct[sk]
        ct+=1
        umdict_ct[sk]=ct
        if ct>1:
         sk="%s%s" %(sk,ct)

        umdict[sk]=uk
        self.param_var_unmasked[sk]=k
        self.param_var_masked[k]=sk
        
     for sk,uk in  umdict.items():
        #print("%s => %s"  %(uk,sk))
        code= code.replace(self.mask(uk),self.mask(sk))

     return code
  
  def unmask(self,code):
     
     code=self.simplenames(code)
     umsk= code.replace(self.vmask,"")
     return umsk
  
  def edge_vartag(self,a,b):
    vartag= "%s__%s_%s"%(b,"p",a)
    return vartag
  
  def add_egdes(self):
 
       for label, element in self.elements.items():
           nid=element.nid
           asso=element.association 
           if asso is not None :
             rent=element.referencedEntity 

             vartag= self.mask(self.edge_vartag(nid,asso))
             eid=self.edge_id(nid,rent.nid)
             self.g.add_edge(nid,rent.nid,association=asso,vartag=vartag,eid=eid)

  def defineVarTagMap(self,tb):

    self.defineVarTag()

    nodes=list(self.g.nodes)
    edges=list(self.g.edges)  

    
    #association=qb.edge_attributes("association")
    evartag=self.edge_attributes("vartag")
  
    vartagMap=dict()
     
    for nid in nodes:
      el=self.nid2e[nid]
      vt=el.vartag
      vartagMap[nid]=vt
       

    for ed in edges:
      if evartag is not None:
        nid1=ed[0] 
        nid2=ed[1] 

        vtn1_s=vartagMap[nid1]
        vtn2_o=vartagMap[nid2]
        
        eid=self.edge_id(nid1,nid2)
        vt_p=evartag[ed]
        vartagMap[eid]=vt_p
        tb.selectpath[eid]=[vtn1_s,vt_p,vtn2_o]
        self.param_var_masked[vtn1_s]=1
        self.param_var_masked[vt_p]=1
        self.param_var_masked[vtn2_o]=1

    return vartagMap
    
  def defineVarTag(self):
    
    ct=0
    vct=0

    for label, element in self.elements.items():
         if ct % 2==0:
           vct+=1
           cti=0
         cti+=1  
         ct+=1
         if isinstance(element,EntityNode):
           vartag=None
           if cti==1:
              vartag=self.mask('s__'+str(vct))
           elif cti==2:
              vartag=self.mask('o__'+str(vct))
              
           element.vartag=vartag

           asso=element.association 
           if asso is not None :
              vartag=self.mask(asso+'__p_'+ element.nid )
              element.associationVarTag[asso]=vartag

           act=0 
           for attn,param in element.filterTypeAttributes.items():
              act+=1
              pvar=self.mask(attn+'__'+str(element.nid)+str(act))
              param['refvar']=pvar
              self.param_var_masked[pvar]=1
              element.filterTypeAttributesVar[attn]=pvar
         else:
           element.vartag=element.nid 
          


  def sparql_generate(self):
    k=TARGET_LANG.SPARQL.value
    if k in self.toolBoxes.keys():
       tb=self.toolBoxes[k]
       query=""
       ct=0
       filters=[]
       a_filters=[]
       t_triples=[]
       template=None
       vct=0

       self.add_egdes()
       nn=self.g.number_of_nodes()

       cct=self.connected_components_count()
       if nn==0 or cct==0:
         #print(self.g)
         raise Exception("no element")
       elif cct>1:
         raise Exception("elements not connected. use 'connectedWith'") 
       
       
       vn=self.defineVarTagMap(tb)
       
       #print(vn) 

       #print(q1)
       sbc=[]
       template=tb.root_query_template()
       
        #print(query)

       for label, element in self.elements.items():
         if ct % 2==0:
           vct+=1
           cti=0
         cti+=1  
         ct+=1
         #print(label," -> ",element)
         if element.validateConstraint()==False:
            raise Exception("%s %s (%s) constraints not validated"  %(element.vartag,element.label,element.__class__)) 
         
         if isinstance(element,EntityNode):
           #print(str(cti),"-->element.targetClass",element.targetClass)
           vartag=None
           if cti==1:
           
              sbc.append(tb.select_byclass( element.targetClass,element.vartag, self.add_children))
               
           elif cti==2:
              filt=tb.a_clause_union(element.vartag,[element.targetClass])
              a_filters.append(filt)
           
           
           asso=element.association 
           if asso is not None :

              vartag=element.associationVarTag[asso]
              filt=tb.filter_clause(vartag,[asso])
              filters.append(filt)
          
         for attn,param in element.filterTypeAttributes.items():
            #pvar=element.filterTypeAttributesVar[attn]
            pvar=param['refvar']
            vartag=element.vartag
            
            tpl=tb.add_triple_palias(vartag,param['name'],pvar)
            t_triples.append(tpl)
            
            filt=tb.filter_clause_param([param],"AND")
            filters.append(filt)

         flcl=self.addFilterNode(tb,element)   
         for flc in flcl:
           #print("-flc-")
           #print(flc)
           filters.append(flc)

       #print(template)       
       #print(a_filters)
       #print(filters)
       q=tb.build( template,a_filters=a_filters,filters=filters,t_triples=t_triples,sbc=sbc)
       q=self.unmask(q)
       q=self.cleanq(q)
       return q
    else:
      return k+"toolbox not available"
  def cleanq(self,iq):
     ql=iq.split('\n')    
     rl=[]
     for l in ql:
        ll=l.strip()
        if len(ll)>0 and  ll.startswith('#') ==False:
          rl.append(ll)
     rs= "\n".join(rl)
     return rs


  def variable_metadata(self):

    vartag2label={}
    vartag2Cls={}
    mask=self.param_var_masked
    for k,element in self.elements.items():
      #print("---- %s=%s"  %(k,  queryBuilder.param_var_masked[v.vartag]) ) 
      mv=mask[element.vartag]
      #label2vartag[k]=mv
      vartag2label[mv]=k
      tc=element.targetClass
      vartag2Cls[mv]=tc
      #print(tc)
  
  
    idxm=self.selectIndexes()
    ml=[]
    for k,idx in idxm.items():
        m={} 
        vartag=mask[k] 
        #print(" - %s=%s"  %(idx,  vartag) )
        cls=None
        label=None
        if vartag in  vartag2Cls.keys():
          cls= vartag2Cls[vartag]
          label=vartag2label[vartag]     
        m['index']=idx 
        m['vartag']=vartag 
        m['class']=cls 
        m['label']=label 
        
        ml.append(m)
    i=-1   
    last_cls=None
    last_cls_ix=None
    for m in ml:
      i+=1
      idx=m['index']
      cls=m['class']
      s=None
      t=None
      if last_cls is not None and cls is None:
        #association detected   
        m['source']=last_cls_ix
        m['target']=idx+1
      else:
        m['source']=None
        m['target']=None
        
      last_cls=cls  
      last_cls_ix=idx
    
    return ml





  def addFilterNode(self,tb,element):
    #print(">>addFilterNode")
    ql=[]
    #mappvar={}
    for fnode in element.filternodes:
       
       paramL=[]
       ix=0
       xop=fnode.indexOperator[ix]
       # at this step do not mix operators
       for an in fnode.attnList:
         
         n=fnode.attnList[ix]   
         v=fnode.attvList[ix]
         o=fnode.opList[ix] 
         param=element.defineParam(n,v,o)

         #fixed : pvar is centralized using  filterTypeAttributesVar
    
         pvar=None
         for uattn,uparam in element.filterTypeAttributes.items():
            upvar=element.filterTypeAttributesVar[uattn]
            if uparam['name']==n:
              #print(uattn)
              #print(uparam)
              #print(upvar)
              pvar=upvar

          
         param['refvar']=pvar
         self.param_var_masked[pvar]=1
         paramL.append(param)
         ix+=1 

       q=""
       #q+=">>>"
       #print(">paramL")
       #print(paramL)
       q+=tb.filter_clause_param(paramL,xop)
       #q+="<<<<"
       ql.append(q)
    return ql
     
  def graph_generate(self):
    return "not yet implemented"
  
  def entity_generate(self):
    return "not yet implemented"
  

class GNodeAbstract():
   def __init__(self):
     self.debug=False
     self.label=None
     self.nid=None
     self.vartag=None

   def validateConstraint(self):    
     #TODO : helper - check the validity of the asso and att filter constraints
     return True

###########################################
class FilterNode():
  def __init__(self):
       
       self.attnList=[]
       self.attvList=[]
       self.opList=[]
       self.indexOperator=dict() 

class FilterOr(FilterNode):
  def __init__(self):
       super().__init__() 
 

  def whereAttribute(self,attn,attv,op):
       self.attnList.append(attn)
       self.attvList.append(attv)
       self.opList.append(op)
       ix=len(self.attnList)-1
       self.indexOperator[ix]="OR"
       return None 


class FilterAnd(FilterNode):
    def __init__(self):
       super().__init__() 

    def whereAttribute(self,attn,attv,op):
       self.attnList.append(attn)
       self.attvList.append(attv)
       self.opList.append(op)
       ix=len(self.attnList)-1
       self.indexOperator[ix]="AND"
       return None      
  
###########################################

class EntityNode(GNodeAbstract):
    
   def __init__(self,label,clsInst):
    super().__init__()
    self.isOperator=False
    self.label = label
    self.inst=clsInst
    self.targetClass = self.inst.__class__.__name__
    self.referencedEntity=None
    self.association=None
    self.filterTypeAttributes={}
    self.filterTypeAttributesVar={}
    self.associationVarTag={}
    self.filternodes=[]

   def defineParam(self,attn,value, operator ):
       param=dict()
       if operator is None:
         operator=OPERATOR.EQ.value
       param['refvar']=None  
       param['name']=attn
       param['value']=value
       param['operator']=operator
       return param
   
   def where(self,fnode:FilterNode):
      self.filternodes.append(fnode)
      return None
   

   def whereAttribute(self,attn,value, operator=None):
       param=self.defineParam(attn,value, operator )
       tat=self.inst.type_attributes()
       if attn not in tat:
          raise Exception("no primitive type  attribute '%s' in class '%s'. Options are %s " %(attn,self.targetClass,tat))
       self.filterTypeAttributes[attn]=param
     
   def connectedWith(self,referencedEntity,association):
    
    oat=self.inst.object_attributes()
    if association not in oat:
          raise Exception("no object  attribute '%s' in class '%s'. Options are %s " %(association,self.targetClass,oat))
    self.referencedEntity=referencedEntity
    self.association=association

   def validateConstraint(self):    
     vc = super().validateConstraint()
     if vc ==False:
       return False
     else:
       #impl here
       return True
