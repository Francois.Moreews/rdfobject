
from .query import OPERATOR
class Toolbox():
    
    def __init__(self ,alias,uri,module):
      self.target_language="SPARQL"
      self.gen_utils=module
      self.alias= alias
      self.uri= uri
      self.use_subclassof=False

       
    
      self.selectpath={}

      self.common_prefixes={
         "rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",
         "rdfs":"http://www.w3.org/2000/01/rdf-schema#",
         "owl":"http://www.w3.org/2002/07/owl#",
         "xsd":"http://www.w3.org/2001/XMLSchema#"
      }

 

      #sparql primitive to build queries
    def prefixes(self,aliases) :
       d={}
       for p in aliases:
         if p in  self.common_prefixes.keys():
           d[p]=self.common_prefixes[p]
       return d
    
    def filter_template(self):
        query_t="FILTER ( %s )"
        return query_t


    def wherepath_frag(self):
       selectpath=""
       i=0
       for k, triple in self.selectpath.items():
          i+=1
          ct=0
          if i>1 :
            selectpath+=".\n    "
          for v in triple:
             selectpath+="?%s " %(v)
       return selectpath

    def selectpath_frag(self):
       selectpath=""
       i=0
       for k, triple in self.selectpath.items():
          i+=1
          ct=0
          for v in triple:
            ct+=1
            if i==1 :
              selectpath+="?%s " %(v)
            elif   ct>1:
               selectpath+="?%s " %(v)   

       return selectpath

    def selectpath_indexes(self):
       idxm=dict()
       i=-1
       for k, triple in self.selectpath.items():
          i+=1
          ct=-1
          for v in triple:
            ct+=1
            idxm[v]=i+ct
       return idxm  
    
    def root_query_template(self,addprefix={}):
        alias=self.alias
        duri=self.uri
        qp=""
        for prefix,uri in addprefix.items():
           qp+="\n    prefix %s: <%s>" %(prefix,uri) 


        selectpath=self.selectpath_frag()
        wherepath=self.wherepath_frag()
        query_t0="""
    %s    
    prefix %s: <%s>
    select %s
    where {
    %s.
    #Q1 
    %s
    #Q2
    %s
    #Q3
    %s
    }
        """
        query_t=query_t0 %(
                            qp,alias,duri,
                            selectpath,
                            wherepath,
                            "%s","%s","%s"
                            )
        
        return query_t
        
    def a_clause_union(self,va,element):

    
        q1=""
        ct=0

        for cln in element:
            ct+=1
            if ct==1:
            
                sep=""
            else:
                sep=" UNION "
            q1+="%s { ?%s a %s:%s } " %(sep, va ,self.alias,cln)
            
        return q1


    def all_children_classes(self,cln,va="s1"):

        if self.use_subclassof==True:
          return self.subclassof( cln,va)
        else:
          return self.all_children_classes_impl( cln,va ) 
        

    def select_byclass(self,cln,va,add_children=False ):        
        if add_children==True:
            #print("add_children==True")
            return self.all_children_classes(cln,va)
        else:
            #print("add_children==False")
            return self.select_byclass_impl(cln,va)
 
    
    def select_byclass_impl(self,cln,va ):        
        
        clsl=[cln]
        q1=self.a_clause_union(va,clsl)  
        rq=self.root_query_template()
        
        query=rq %("%s",q1,"%s")
        
        return query
    
    def all_children_classes_impl(self,cln,va ):        
        
        clsl=[cln]

        children=self.gen_utils.class_children(cln)
        clsl.extend(children)
        #print("all_children_classes_impl:",clsl)

        q1=self.a_clause_union(va,clsl)  
        
        return q1

    def subclassof(self,cln,va):
 
        q1="""{ ?%s rdf:type ?type%s.
        ?type%s rdfs:subClassOf  %s:%s }""" %( va ,va,va,self.alias,cln)   
    
        rq=self.root_query_template(self.prefixes(['rdf','rdfs']))
        
        query=rq %("%s",q1,"%s")
        return query
    def filter_clause_param(self, paramL,opcl="AND"):
        
        #print("filter_clause_param")
        #print(paramL)
        #print(opcl)

        F1="FILTER ("
        PEND=" )"
        F2=PEND
        AND_STR=" && "
        OR_STR=" || "

        if opcl=="AND":
           OP_STR=AND_STR
        elif opcl=="OR":
           OP_STR=OR_STR

        cond=""
        i=0
      
        for param in paramL:
          if i>0  :
             cond+=OP_STR
          fc=self.filter_clause_param_impl( param) 
          #print("=%s=" %fc)
          cond+=fc
          i+=1   

        q="%s %s %s" %(F1,cond,F2) 
        return q

    def filter_clause_param_impl(self, param):
        #print(param)
        
        s=param["refvar"]

        op="="
        if param['operator']==OPERATOR.EQ.value:
          op="="
        elif param['operator']==OPERATOR.NE.value:
          op="!="
        elif param['operator']==OPERATOR.GT.value:
          op=">"
        elif param['operator']==OPERATOR.LT.value:
          op="<"
        val=param['value']
        attn=param['name']
        
        
        if param['operator']==OPERATOR.STARTSWITH.value:
          cond="STRSTARTS(STR(?%s),'%s')" %(s,val)
        elif param['operator']==OPERATOR.ENDSWITH.value:
          cond="STRENDS(STR(?%s),'%s')" %(s,val)
        elif param['operator']==OPERATOR.CONTAINS.value:
          cond="CONTAINS(?%s),'%s')" %(s,val)
        else:
          opval=""" %s  '%s' """ %(op,val)
          cond="?%s %s" %( s,opval ) 
          
        #print("================>%s<=======" %cond)
        return cond
    
    def add_triple_palias(self,s,p,o):
       pa="%s:%s"%(self.alias,p)
       return self.add_triple("?"+s,pa,"?"+o)
    def add_triple(self,s,p,o):
       q="%s %s %s" %(s,p,o)
       return q
    
    def filter_clause(self,vartag,cln):
       pvart="?"+ vartag 
       return self.filter_clause_impl(pvart,cln)
    
    def filter_clause_impl(self,p,cln):
        cl=list()   
        if isinstance(cln, list):
          for c in cln:
            cl.append(c)   
        else:
          cl.append(cln)     

        q=""  
        ct=0
        for c in cl:
            ct+=1
            if ct==1:
                sep=""
            else:
                sep=" || "
            q+= "%s %s =  %s:%s " %(sep,p,self.alias,c)
            
        q= self.filter_template() %(q)
        return q

    def build(self,template,a_filters=list(),filters=list(),t_triples=list(),sbc=[]):
        q1=""
        if len(sbc)>0:
           for sb in sbc:
              q1+=sb

        template=template %("%s",q1,"%s")

        aq=self.clause_frag(a_filters,parenthesis=False, addsep=True)
        tq=self.clause_frag(t_triples,parenthesis=True, addsep=True)
        fq=self.clause_frag(filters,parenthesis=False, addsep=False)
        #print("--------------")
        #print("!!!!!!!!!!template")
        #print(template)
        #print("aq")
        #print(aq)
        #print("tq")
        #print(tq)
        #print("fq")
        #print(fq)    
        #print("--------------")
        aq="#aq\n"+aq+"#tq\n\n"+tq+"\n"  

        query=template %(aq,fq)        
        return query
    def add_filt_frag(self,fi,parenthesis):
       if parenthesis==True:
         fg="{ %s }" %(fi) 
       else:
         fg=" %s " %(fi) 
       return fg
    
    def clause_frag(self,afilters,parenthesis=False, addsep=True):


        fq=""
        ac=0
        EP="."
        SP=EP+"\n"
        
          
        if afilters is not None:
           for fi in afilters:
            ac+=1
            if ac==1:
              fq+=self.add_filt_frag(fi,parenthesis)
            else:
              if fi.strip().endswith(EP)==False: 
                if addsep==True:
                  sep=SP
                else:
                  sep=""   
              else:
                sep=""
              fq+=sep+self.add_filt_frag(fi,parenthesis)
             
            if fq.strip().endswith(EP)==False:   
              fq=fq+SP 
        return fq
        #
    #
    #         * From PhysicalEntity to the downstream Conversion.
    #         * @return generative constraint to get the Conversion that the PhysicalEntity is a controller
    #         */
    #        public static Constraint peToControlledConv()
    #        {
    #                return new PathConstraint("PhysicalEntity/controllerOf/controlled*:Conversion");
    #        }

    #Conversion

    #
    # 
    #      * From EntityReference to the member PhysicalEntity
    #        * @return generative constraint to get to the member PhysicalEntity of the EntityReference
    #         */
    #        public static Constraint erToPE()
    #        {
    #              return new PathConstraint("EntityReference/entityReferenceOf");
    #        }
    #
    #
    #
    #

    # 